#!/bin/sh
CURL_CMD='curl -i -u guest:guest -H "content-type:application/json" -X '
BASE_URL='http://localhost:15672/api/'
echo Create exchange
echo ${CURL_CMD} PUT -d @$(pwd)/ex_input.json ${BASE_URL}exchanges/%2F/ex_input
echo ${CURL_CMD} PUT -d @$(pwd)/queue_liana.json ${BASE_URL}queues/%2F/liana
echo ${CURL_CMD} PUT -d @$(pwd)/queue_solis.json ${BASE_URL}queues/%2F/solis
echo ${CURL_CMD} POST -d @$(pwd)/queue_liana_bindings.json ${BASE_URL}bindings/%2F/e/ex_input/q/liana
echo ${CURL_CMD} POST -d @$(pwd)/queue_solis_bindings.json ${BASE_URL}bindings/%2F/e/ex_input/q/solis
