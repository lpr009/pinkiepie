# Setup environment
## Docker

    https://docs.docker.com/engine/install/ubuntu/
    https://techexpert.tips/fr/docker-fr/docker-configuration-proxy-sur-ubuntu-linux/

## RabbitMQ
```bash
sudo docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management
```
Valider que RabbitMQ fonctionne en se connectant à l’interface d’adiministration en web http://localhost:15672 **guest:guest**

Configure exchange, queue and binding
```bash
pinkiepie/test/init_rmq.sh
```
## Pinkiepie
Create and adapt Pinkiepie configuration file
```bash
cargo run -- --example-conf > /usr/local/etc/pinkiepie
```

Run the server with Trace level verbose mode
```bash
cargo run -- --verbose Trace | grep -v "lapin::\|hyper::\|mio::pol"
```

# Some usefull commands
```bash
# POST request with demo.xml
curl -i -X POST -d @/home/xxxxx/dev/pinkiepie/test/demo.xml localhost:7878/v1/billing
# GET request
curl -i localhost:7878/v1/billing
curl -i localhost:7878/v1/employee
```
