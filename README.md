> Pinkie Pie, who banished fear by giggling in the face of danger, represents the spirit of... laughter!

This is an example/template projet that uses iron (with router and bodyparser) and lapin crates to build a micro REST application able to take some input (via `POST` or `GET` HTTP verb), check the content, validate it (must be XML) and put the result in the corresponding AMQP/RabbitMQ Exchange. And for `GET` wait for response from the remote RPC producer.

The objective is to build an HTTP server that binds on a local port and accept POST on an URI. If the posted data is an XML UTF-8 string, it is sent via RabbitMQ to a particular exchange/routing key/vhost via user/pass.

Whatever happens behind the scene (connection lost to RabbitMQ, channel closed, etc…), the Web part will still be able to respond :
* `200 (OK)` is the expected code if everything has been posted properly
* `417 (Expectation Failed)` if the POST body is empty
* `415 (Unsupported Media Type)` if the POST body is not UTF-8 XML
* `500 (InternalServerError)` if something goes in between the treatment of the HTTP request and RabbitMQ
* `503 (Service Unavailable)` if something goes wrong with RabbitMQ

It is, of course, possible to parse the body as JSON/Simple text if necessary.

The server is able to handle 8 concurrent connections.

# Usage

```
pinkiepie --help
```

# Monitoring

```bash
curl -i http://localhost:7878/status
```

Return:
* `200 (OK)` if Pinkiepie is currently working
* `503 (Service Unavailable)` if Pinkiepie feels sick

# Configuration file
Default configuration `/usr/local/etc/pinkiepie.conf`
```toml
# HTTP server
[http]
port = 7878
address = "localhost"

# RabbitMQ
[rmq]
rabbitmq_host = "localhost"
rabbitmq_user = "guest"
rabbitmq_pass = "guest"
#rabbitmq_exchange = "ex_input" # Optionnal default empty
#rabbitmq_rpc_queue = "rpc_qu"  # Optionnal default qu_rpc_<random UUID>
#rabbitmq_rpc_timeout = "60"	# Optionnal default 60s (not implemented)
#rabbitmq_vhost = "<vhost>"     # Optionnal default no vhost
#rabbitmq_tls = true|false      # Optionnal default false
#rabbitmq_port = 5672           # Optionnal default 5672
#rabbitmq_ack = true|false      # Optionnal default false (not implemented)

# List available endpoints and their routing key
[endpoints."v1/employee"]
route = "solis"
http_method = "GET"
producer_code = "tld_employee"

[endpoints."v3/employee]
route = "solis"
http_method = "GET"
producer_code = "tld_employee_new"

[endpoints."v1/billing]
route = "liana"
http_method = "POST"
producer_code = ""
```

# RabbitMQ messaging
Values sent from Pinkiepie to RMQ producer:
* api_endpoint		: the called API endpoint
* query_component	: the query component of the called API
* correlation_id	: the correlation ID needed to redirect the result to the API client
* producer_code		: identify producer on a multi-producer destination system (eg. on the main ETL server it may happens to have different type of producer worker: v1 employee, v2 employee, v1 company organization...)
Others values :
* correlation_id	: compulsory otherwise Pinkiepie can’t reply to the API’s client
* content_type		: optionnal cf. IANA (only from RMQ producer to Pinkiepie)

# Architecture
Iron HTTP server is running 8 listening threads per cpu waiting  for new connection. Thus Pinkiepie can handle 8 simultanous HTTP requests per CPU.
The following HTTP verbs are currently supported: HTTP POST and HTTP GET.
Default timeouts keep alive: 5s, read: 30s and write 1s. Rereferenc:  https://docs.rs/iron/0.6.1/src/iron/iron.rs.html#39 .

At startup Sender and Receiver tasks are spawn.

The Sender task is an abstraction layer, it hold and manage all the necessary object and configuration to interact with RMQ.
Communication between the HTTP server and Sender is made with an mspc channel.
Once proccessed by the HTTP server the request is transformed and forwarded through the mspc channel to the sender Task.

The Receiver task is more and less the same abstraction layer as the Sender task.
At the following difference, it’s not listening to the mpsc channel but to RMQ directly.
Foreach message it spawn a new task which handle and forward response message to the corresponding HTTP server thread.

Quirks:
* Iron is running with futures https://docs.rs/futures and the remaining of the program with tokio https://docs.rs/tokio
* Since using async there is no guarantee that a message is successfully sent to the RMQ producer and vice-versa (it would require a lot more work to implement that and then reduce the performance)
 

[Internal process diagram](https://viewer.diagrams.net/?highlight=0000ff&edit=_blank&layers=1&nav=1&title=Untitled%20Diagram.drawio#R7VltT9swEP41lTYkqrw0bfkILYxJIPHSafBpcpNrYkjjyHFpu1%2B%2FS%2BM0cZ2WAC2FCQmEfTk79nP33F2Oht0bz35wEgeXzIOwYRnerGH3G5Zltiyrkf4Y3jyTdDp2JvA59aRSIbilf0EKDSmdUA8SRVEwFgoaq0KXRRG4QpERztlUVRuxUH1rTHzQBLcuCXXpb%2BqJIJN2rU4hPwfqB%2FmbzfZR9mRMcmV5kyQgHpuWRPZpw%2B5xxkQ2Gs96EKbg5bhk687WPF0ejEMk6izoD3t48Os%2Fw%2BPJYDB%2BcMWv9t2hKU%2F7RMKJvLE8rZjnEPicTWKpBlzArAp4MszVDf1g5vK66CfAxiD4HFXkRrZckXuInE4LuJ1806AEdStXJNLE%2FnLnAgUcSCBeAkr7eVAQk8iDdBejYZ9MAyrgNiZu%2BnSKTEBZIMb41r6Jw7XglUHaYB8dOomVVRcrZ2dYdTWsrmj0SCGmoIGG1xcqMong7BF6LGQcJRGLUPNkRMNwRURC6kc4dREuQPlJCiZFmh7LB2PqeelrKk2hGmv71ijB365A39oV%2BC0N%2B3GcuCg5aDZNDX3wMLLJKeMiYD6LSHhaSFdwKnQuGIulwR5AiLkM02QimGpOmFFxVxrfp1s1HTnrz%2BTOi8k8n0QIxV15UlqVTotli9lcMWN6qbVGzDFgE%2B7CJrvKxEK4D5vsb1Wbn0NIBH1Sz7F9plUEpXaY8smjTzj00%2BH5YHCFSof4%2B5OzKNcY8lwhl%2BARSssqdvrWTREJOBDv%2B7pFqw4WhpiW15Gw5CYkibNcPaKz1N3qUXJzFnHULGK29NBo2hXkXAq3bzBbs9gFiSkaxTC%2F%2BPkSflo1%2BWka1S7yPgS16hD0FtCCXFJ0wB4pw7%2FSLeoy01xgkTwuqsyFP63w9Hm6fyTmWkc1qJsX3Ap1c%2BHWTdneJzkLQt6XnlSTc4sks2uSzNknxzp1ONaHEHwiIAu0B3U5gCBekCF%2Bv6oeX7vy5JDQv%2FI7KLVNzGgkFgA4Jw2nv4ksdT6i1jvqWmYdGk0s%2FY8UduV1RG17yd2v0tuUVNholKCfrBp0eYg35E09kH7lzZ1S2lwToN%2BH03qZVMHpG3ABT7Ixc76myv3fcmm3o7DdNjr7LoO7e82lryJswdFlBn4fwjo1CWvttdDVe4YsgiRgYlOE%2FnSptftcarVbpqMWrh89szovjLRnEzHhF3RRSZWKqsO3fLKceXKjT9FVMFfaCq2qeGpUxVNjZ20FQ7PizeW1Btv2G9TP9V9WvuK63aajY1XVnd5dB0bvmX1EqGynwqneFShL96kKFpulwmuwqJLwZbLjmHUbZfH0KZhtqkZYgrsLZuO0%2BEdfFs2Lf5fap%2F8A)

# Test
See [test documentation](https://framagit.org/lpr009/pinkiepie/-/blob/master/test/README.md)

# Evolution
* Replace Iron (which does not seems to be maintened anymore) with Gotham or Arctix
* producer_code must be optionnal value
