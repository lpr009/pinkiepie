use clap::{Arg, App};
use core::str::FromStr;
use log::LevelFilter;
use serde_derive::{Serialize, Deserialize};
use std::{collections::HashMap, fmt};
use simple_logger::SimpleLogger;
use uuid::Uuid;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Config {
    #[serde(default = "Config::default_http")]
    pub http: ConfigHTTP,
    pub rmq: ConfigRMQ,
    pub endpoints: HashMap<String, ApiEndpoint>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ApiEndpoint {
    pub route: String,
    pub http_method: String, // Trait xSerialize not implemented for iron::method::Method
    pub producer_code: String, 
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ConfigRMQ {
    pub rabbitmq_host: String,
    #[serde(default = "Config::default_rabbitmq_vhost")]
    pub rabbitmq_vhost: String,
    #[serde(default = "Config::default_rabbitmq_tls")]
    pub rabbitmq_tls: bool,
    pub rabbitmq_user: String,
    pub rabbitmq_pass: String,
    #[serde(default = "Config::default_rabbitmq_port")]
    pub rabbitmq_port: u16,
    pub rabbitmq_exchange: Option<String>,
    #[serde(default = "Config::default_rabbitmq_ack")]
    pub rabbitmq_ack: bool,
     #[serde(default = "Config::default_rabbitmq_rpc_queue")]
    pub rabbitmq_rpc_queue: String,
}


#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ConfigHTTP {
    #[serde(default = "Config::default_port")]
    pub port: u16,
    #[serde(default = "Config::default_address")]
    pub address: String,
}

impl ConfigRMQ {

    pub fn get_rmq_url(&self) -> String {
        let mut amqp_uri: String;

        if self.rabbitmq_tls {
            amqp_uri = "amqps://".to_string();
        } else {
            amqp_uri = "amqp://".to_string();
        }

        amqp_uri.push_str(&self.rabbitmq_user);
        amqp_uri.push_str(":");
        amqp_uri.push_str(&self.rabbitmq_pass);
        amqp_uri.push_str("@");
        amqp_uri.push_str(&self.rabbitmq_host);
        amqp_uri.push_str(":");
        amqp_uri.push_str(&self.rabbitmq_port.to_string());
        amqp_uri.push_str("/");
        amqp_uri.push_str(&self.rabbitmq_vhost); 
        
        amqp_uri
    }
}

impl Config {
    pub fn default_rabbitmq_tls() -> bool { false }
    pub fn default_rabbitmq_port() -> u16 { 5672 }
    pub fn default_rabbitmq_vhost() -> String { String::from("%2f") }
    pub fn default_rabbitmq_ack() -> bool { false }
    pub fn default_rabbitmq_rpc_queue() -> String { format!("qu_rpc_{}", Uuid::new_v4()) }
    pub fn default_port() -> u16 { 7878 }
    pub fn default_address() -> String { String::from("localhost") }
    pub fn default_http() -> ConfigHTTP { ConfigHTTP { port: Config::default_port(), address: Config::default_address() } }

    pub fn new() -> Config {
        let matches = App::new(env!("CARGO_PKG_NAME"))
                    .version(env!("CARGO_PKG_VERSION"))
                    .about("A daemon that uses iron and lapin to process POST requests to RabbitMQ messages")
                    .arg(Arg::with_name("pinkiepie.conf")
                         .short("c")
                         .long("pinkiepie.conf")
                         .value_name("CONF_FILE")
                         .help("path to configuration file (default to /usr/local/etc/pinkiepie)")
                         .takes_value(true)
                         .display_order(1))
                    .arg(Arg::with_name("print-conf")
                         .short("p")
                         .long("print-conf")
                         .help("print the configuration file to the console"))
                    .arg(Arg::with_name("example-conf")
                         .short("e")
                         .long("example-conf")
                         .help("print an example configuration file to the console"))
                    .arg(Arg::with_name("verbose_mode")
                         .long("verbose")
                         .value_name("VERBOSE_MODE")
                         .help("verbosity level: Off, Error, Warn, Info, Debug, Trace. Default is Info.")
                         .takes_value(true))
                    .get_matches();

        
        // Print a dummy configuration file and exit
        if matches.is_present("example-conf") {
            println!("{}", Config::new_dummy());
            std::process::exit(0);
        }

        let conf_path_file = matches.value_of("pinkiepie.conf").unwrap_or("/usr/local/etc/pinkiepie");
        let conf: Config = toml::from_str(&std::fs::read_to_string(conf_path_file).expect("Cannot read config file")).expect("Invalid configuration file");

        // Print the current configuration file
        if matches.is_present("print-conf") {
            println!("{}", &conf);
        }

        // Activate logging
        let mut level = LevelFilter::Info;
        if matches.is_present("verbose_mode") {
            match LevelFilter::from_str(matches.value_of("verbose_mode").unwrap()) {
                Ok(l) => { level = l; },
                Err(e) => { log::error!("Unknown verbose level filter: {}", e); }
            }
        } 
        SimpleLogger::new().with_level(level).init().unwrap();

        conf
    }
   

    /// Generate a dummy configuration file for example
    pub fn new_dummy() -> Config {
        let api_a = ApiEndpoint { route: String::from("rmq_route_to_solis"), http_method: String::from("GET"), producer_code: String::from("tld_employees") };
        let api_b = ApiEndpoint { route: String::from("rmq_route_to_liana"), http_method: String::from("POST"), producer_code: String::from("tld_billing")};
        let mut endpoints = HashMap::new();
        endpoints.insert(String::from("v1/get-employees"), api_a);
        endpoints.insert(String::from("v1/post-bills"), api_b);

        Config {
            http: ConfigHTTP { port: 7878, address: String::from("localhost") },
            rmq: ConfigRMQ {
                rabbitmq_host: String::from("localhost"),
                rabbitmq_user: String::from("guest"),
                rabbitmq_pass: String::from("guest"),
                rabbitmq_exchange: Some(String::from("rmq_exchange_to")),
                rabbitmq_ack: false,
                rabbitmq_tls: false,
                rabbitmq_vhost: String::from(""),
                rabbitmq_port: 1625,
                rabbitmq_rpc_queue: String::from("rpc_queue"),
            },
            endpoints: endpoints,
        }
    }
}

impl fmt::Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut conf = self.clone();
        conf.rmq.rabbitmq_pass = String::from("****");

        write!(f, "{}", toml::to_string(&conf).unwrap())    
    }
}
