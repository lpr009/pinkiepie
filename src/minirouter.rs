use crate::config::*;
use crate::pinkiepieerror::PinkiepieError;
use iron::headers::ContentType;
use iron::method::Method;
use lapin::{BasicProperties};
use lapin::types::{AMQPValue, FieldTable, LongString, ShortString};
use std::collections::HashMap;
use std::error::Error;
use std::str::FromStr;
use std::sync::Arc;
use std::convert::Into;
use std::time::Instant;
use tokio::runtime::Runtime;
use tokio::sync::{ mpsc, oneshot, RwLock };
use tokio::time::{sleep, Duration};
use uuid::Uuid;

type Tx = mpsc::UnboundedSender<PinkiepieMessage>;
type OneshotSender = oneshot::Sender<Option<PinkiepieMessageResponse>>;
type Iprt = Arc<RwLock<HashMap<String, PinkiepieMessageMetadata>>>;

pub struct MiniRouter {
    routes: HashMap<String, ApiEndpoint>,
    sender_tx: Tx,
    iprt: Iprt,
    cleaning_day: Runtime
}

impl MiniRouter {

    pub fn new(routes: HashMap<String, ApiEndpoint>, tx: Tx, iprt: Iprt) -> MiniRouter {
        MiniRouter {
            routes: routes,
            sender_tx: tx,
            iprt: iprt,
            cleaning_day: Runtime::new().unwrap(),
        }
    }

    /// Get API’s producer code 
    pub fn get_producer_code(&self, key: &String) -> Result<String, Box<dyn Error>> {
        match self.routes.get(key) {
            Some(e) => return Ok(e.producer_code.clone()),
            None => return Err(Box::new(PinkiepieError::new(&format!("No producer code available for {}", key))))
        }
    }

    /// Get API’s route
    pub fn get_route(&self, key: &String) -> Result<String, Box<dyn Error>> {
        match self.routes.get(key) {
            Some(e) => return Ok(e.route.clone()),
            None => return Err(Box::new(PinkiepieError::new(&format!("No route available for {}", key))))
        }
    }
    
    /// Get API’s method
    pub fn get_method(&self, key: &String) -> Result<String, Box<dyn Error>> {
        match self.routes.get(key) {
            Some(e) => return Ok(e.http_method.clone()),
            None => return Err(Box::new(PinkiepieError::new(&format!("No HTTP method available for {}", key))))
        }
    }
    
    /// Validate API existence and method then return tuple routing key, method and producer code
    pub fn validate_api_and_method(&self, key: &String, method: &String) -> Result<(String, Method, String), Box<dyn Error>> {
        let (r, m, p) = (self.get_route(&key)?, self.get_method(&key)?, self.get_producer_code(&key)?);
        if m.ne(method) { return Err(Box::new(PinkiepieError::new(&format!("HTTP method unauthorized")))); }
        Ok((r, Method::from_str(m.as_str())?, p))
    }

    /// Publish message to AMQ Sender handler
    pub fn publish_message(&self, msg: PinkiepieMessage, resp_tx: Option<OneshotSender>) -> Result<(), Box<dyn Error>> {
        if let Some(tx) = resp_tx {
            self.update_iprt(msg.get_uuid().clone(), tx);
        }
        // Publish message on best effort
        self.sender_tx.send(msg)?;
        Ok(())
    }

    /// Update the IPRT
    fn update_iprt(&self, uuid: String, resp_tx: OneshotSender) {
        Runtime::new().unwrap().block_on( async move { 
            if let mut iprt = self.iprt.clone().write().await {
                log::debug!("Update IPRT for {}", &uuid);
                iprt.insert(uuid, PinkiepieMessageMetadata { resp_tx: resp_tx,
                                                             timestamp: Instant::now() });
            } else {
                log::error!("Unable to get write lock to receiver IPRT");
            }
        });
    }

    /// Clean the IPRT table (link between HTTP server and RabbitMQ Receiver)
    /// Removing the entry close de the channel between HTTP server and RMQ Receiver which leads
    /// the HTTP server to close the connection since nothink else is blocking
    pub fn start_cleaning_day(&self) {
        let iprt_day = self.iprt.clone();

         self.cleaning_day.spawn( async move {
             log::debug!("Thread cleaning day spawned");
             loop {
                 log::debug!("Time for Cleaning day");
                 if let mut map = iprt_day.write().await {
                     map.retain(|k, v| { 
                         if v.timestamp.elapsed().as_secs() >= 600 { // Remove the entry after an arbitrary 10 minutes
                             log::debug!("Remove {} from IPRT", &k);
                             return false
                         }
                         log::debug!("Keep {} from IPRT", &k);
                         true
                     });
                 } else {
                     log::error!("Unable to get write lock to receiver IPRT");
                 }
                 sleep(Duration::from_secs(300)).await;
             }
         });
    }
}


#[derive(Debug)]
pub struct PinkiepieMessageMetadata {
    pub resp_tx: OneshotSender,
    pub timestamp: Instant,
}

#[derive(Debug)]
pub struct PinkiepieMessage {
	body: String,
    rmq_routing_key: String,
    rmq_response_queue: String,
    api_endpoint: String,
    api_query_component: Option<String>,
	timestamp: Instant, // For cleaning day 
    uuid: String, // Uniq ID for the message (used as correlation_id)
    producer_code: String,
}

impl PinkiepieMessage {
    pub fn new(body: String, 
               route: String, 
               queue: String,
               api: String,
               query: Option<String>,
               producer: String,
                ) -> PinkiepieMessage {
        PinkiepieMessage {
    	    body: body,
    	    rmq_routing_key: route,
    	    rmq_response_queue: queue,
    	    api_endpoint: api,
    	    api_query_component: query,
    	    timestamp: Instant::now(),
    	    uuid: Uuid::new_v4().to_string(), 
            producer_code: producer,
        }
    }

    pub fn get_body(&self) -> String {
        self.body.clone()
    }

    pub fn get_routing_key(&self) -> String {
        self.rmq_routing_key.clone()
    }

    pub fn get_uuid(&self) -> String {
        self.uuid.clone()
    }

    pub fn get_basic_properties(&self) -> BasicProperties {
        let mut ft = FieldTable::default();

        match &self.api_query_component {
            None => (),
            Some(e) => ft.insert("query_component".into(), 
                                 AMQPValue::LongString(LongString::from(e.to_owned()))),
        }
        
        ft.insert("api_endpoint".into(), AMQPValue::LongString(LongString::from(self.api_endpoint.clone())));
        ft.insert("producer_code".into(), AMQPValue::LongString(LongString::from(self.producer_code.clone())));
                  

        BasicProperties::default()
            .with_delivery_mode(2)
            .with_reply_to(ShortString::from(self.rmq_response_queue.to_owned()))
            .with_headers(ft)
            .with_correlation_id(ShortString::from(self.uuid.to_owned()))
    }
}


#[derive(Debug)]
pub struct PinkiepieMessageResponse {
    uuid: String,
    body: Option<Vec<u8>>,
    content_type: ContentType
}

impl PinkiepieMessageResponse {
    pub fn new(uuid: String, body: Option<Vec<u8>>, content_type: Option<String>) -> PinkiepieMessageResponse {
        PinkiepieMessageResponse {
            uuid: uuid,
            body: body,
            content_type: PinkiepieMessageResponse::string_to_content_type(content_type) 
        }
    }

    fn string_to_content_type(cts: Option<String>) -> ContentType {
        if let Some(e) = cts {
            if let Ok(ct) = e.parse() {
                return ContentType(ct);
            } 
        }
        ContentType::plaintext()
    }

    pub fn get_content_type(&self)  -> ContentType {
        self.content_type.clone()
    }

    pub fn take_body(&mut self) -> Vec<u8> {
        match self.body.take() {
            Some(e) => e,
            None => String::from("").as_bytes().to_vec(),
        }
    }

}
