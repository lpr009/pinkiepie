use crate::config::*;
use crate::minirouter::*;
use crate::pinkiepieerror::PinkiepieError;
use iron::{Handler, headers::ContentType, method::Method, modifiers::Header, prelude::*, status::Status};
use std::{error::Error, collections::HashMap, convert::*, str, sync::Arc};
use tokio::{runtime::Runtime, sync::{ mpsc, oneshot, RwLock}};
use xml::reader::EventReader;

type Tx = mpsc::UnboundedSender<PinkiepieMessage>;
type Iprt = Arc<RwLock<HashMap<String, PinkiepieMessageMetadata>>>;
type PStatus = Arc<RwLock<Result<(), Box<PinkiepieError>>>>;

/// Handle HTTP response
fn response_handling<T>(s: Status, m: T, h: Header<ContentType>) -> Response 
    where T: std::fmt::Debug + iron::modifier::Modifier<iron::Response>
{
        if log::max_level().gt(&log::LevelFilter::Info) {
            log::debug!("HTTP response status:{}, Message: {:?}", s, m);
        } else {
            log::info!("HTTP response status:{}",s);
        }
        Response::with((s, m, h))
}

/// Abstraction for response_handling - with default Header if needed
macro_rules! response_handling {
    ($s:expr, $m:expr) => {
        response_handling($s, $m, Header(ContentType::plaintext())) 
    };
    ($s:expr, $m:expr, $h:expr) => {
        response_handling($s, $m, $h)
    };
}

/// Validate the XML file
fn valid_xml (xml: &str) -> Result<(), Box<dyn Error>> {
    let p = EventReader::from_str(xml);

    for l in p {
        if let Err(e) = l {
            return Err(Box::new(e));
        }
    }

    Ok(())
}

/// HTTP handler
pub struct HTTPHandler {
    conf_rmq: ConfigRMQ,
    minirouter: MiniRouter,
    pinkiepie_status: PinkiepieStatus,
}

impl HTTPHandler {
    pub fn new(conf: Config, sender_tx: Tx, iprt: Iprt, status: PinkiepieStatus) -> HTTPHandler {
        let mr = MiniRouter::new(conf.endpoints, sender_tx, iprt);
        mr.start_cleaning_day();

        HTTPHandler {
            conf_rmq: conf.rmq,
            minirouter: mr,
            pinkiepie_status: status,
        }
    }

    /// Return the current application status - important for supervision
    fn status(&self) -> IronResult<Response> {
        match self.pinkiepie_status.is_pinkipie_ok() {
            Ok(_) => Ok(response_handling!(Status::Ok, format!("{} {}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION")))),
            Err(_e) => Ok(response_handling!(Status::ServiceUnavailable, format!("{} {} - KO please check the logs",env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION")))),
        }
    }


}

impl Handler for HTTPHandler {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        let query_component: Option<String> = req.url.query().map(String::from);
        let called_api = req.url.path().join("/");
        let routing_key: String;
        let producer_code: String;
        let method: Method;
        
        if called_api.eq("status") {
            return self.status() 
        }

        match self.minirouter.validate_api_and_method(&called_api, &req.method.to_string()) {
            Ok((r,m, p)) => {
                routing_key = r;
                method = m;
                producer_code = p;
            },
            Err(e) => return Ok(response_handling!(Status::ServiceUnavailable, format!("Unavailable method or API: {}", e).as_str()))
        }

        log::info!("Called API {:#?} - Query {:?} - Method {} - RMQ route {}", &called_api, req.url.query(), method, routing_key);

        // Prepare body message to be sent to RabbitMQ
        // Empty body foh HTTP GET
        // Supplied UTF-8 XML validated body for HTTP POST
        let mut body = String::from("");
        if Method::Post.eq(&method) {
			body = match req.get::<bodyparser::Raw>() {
                Ok(Some(body)) => body,
                Ok(None) => return Ok(response_handling!(Status::ExpectationFailed, format!("Body appears to be empty").as_str())),
                Err(e) => return Ok(response_handling!(Status::UnsupportedMediaType, format!("Body cannot be parsed: {}", e).as_str())),
            };
/* 20211014 suppress xml validation the program must no longer support only xml but csv too
			match valid_xml(&body) {
                Ok(_) => (),
                Err(e) => return Ok(response_handling!(Status::UnsupportedMediaType, format!("Body is not a valid XML: {}", e).as_str())),
            };*/
        } 
      
        // Prepare the message envelop
        let (resp_tx, resp_rx) = oneshot::channel();
        let msg = PinkiepieMessage::new(body, routing_key, self.conf_rmq.rabbitmq_rpc_queue.clone(), called_api, query_component, producer_code);

        // Send message
        match self.minirouter.publish_message(msg, Some(resp_tx)) {
            Ok(_) => (),
            Err(e) => return Ok(response_handling!(Status::InternalServerError, format!("Error while publishing message: {}", e).as_str())),
        };

        // Prepare the response
        let mut response = String::from("OK").as_bytes().to_vec(); // Default response message
        let mut content_type = ContentType::plaintext(); // Default response content type

        // Quirks: Spawn a Tokio runtime on a future runtime
        if Method::Get.eq(&method) {
            match Runtime::new().unwrap().block_on( async move { resp_rx.await }) {
                Ok(e) => { // Option<_>>
                    if let Some(mut r) = e {
                        response = r.take_body();
                        content_type = r.get_content_type();
                    } else { 
                        return Ok(response_handling!(Status::InternalServerError, format!("Unable to get an answer").as_str())); 
                    }
                },
                Err(e) => return Ok(response_handling!(Status::InternalServerError, format!("Unable to get an answer {}", e).as_str())),
            };
        }

        Ok(response_handling(Status::Ok, response, Header(content_type)))
    }

}

/// Hold PinkiepieStatus
pub struct PinkiepieStatus {
    is_publisher_ok: PStatus,
    is_receiver_ok: PStatus
}

impl PinkiepieStatus {

    /// Default struct creator
    pub fn new() -> PinkiepieStatus {
        PinkiepieStatus {
            is_publisher_ok: Arc::new(RwLock::new(Ok(()))),
            is_receiver_ok: Arc::new(RwLock::new(Ok(())))
        }
    }

    /// Getter for is_publisher_ok
    pub fn get_is_publisher_ok(&self) -> PStatus {
        return self.is_publisher_ok.clone()
    }

    /// Getter for is_receiver_ok
    pub fn get_is_receiver_ok(&self) -> PStatus {
        return self.is_receiver_ok.clone()
    }

    /// Get Pinkiepie global status
    pub fn is_pinkipie_ok(&self) -> Result<(), Box<PinkiepieError>> {
        //let pub_state =  self.is_publisher_ok.try_read().unwrap();
        let rec_state = self.is_receiver_ok.try_read().unwrap();

        //match pub_state.as_ref().and(rec_state.as_ref()) { // Actually receiver is recovering
        //regurlarly instead of publisher which is only recovering during an HTTP request (not very
        //accurate for monitoring that’s why I only rely on receiver state
        match rec_state.as_ref() {
            Ok(_) => Ok(()),
            Err(e) => Err(Box::new(*e.clone())),
        }
    }
}
