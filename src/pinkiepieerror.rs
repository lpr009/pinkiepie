use std::fmt;

#[derive(Debug, Clone)]
pub struct PinkiepieError {
    details: String,
}

impl PinkiepieError {
    pub fn new(msg: &str) -> PinkiepieError {
        PinkiepieError {
            details: msg.to_string(),
        }
    }
}

impl fmt::Display for PinkiepieError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl std::error::Error for PinkiepieError {
    fn description(&self) -> &str {
        &self.details
    }
}
