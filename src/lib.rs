mod config;
mod httphandler;
mod rmqhandler;
mod pinkiepieerror;
mod minirouter;

use crate::config::*;
use crate::httphandler::{HTTPHandler, PinkiepieStatus};
use crate::rmqhandler::*;
use iron::prelude::*;
use std::collections::HashMap;
use std::sync::Arc;
use tokio::sync::{mpsc, RwLock} ;

#[tokio::main]
pub async fn run() {
    let conf = Config::new(); // Create the configuration structure
    let conf_http = conf.http.clone(); // Create the configuration for the HTTP server
    let conf_rmq = conf.rmq.clone(); // Create the configuration for the RMQ handler
    let (sender_tx, sender_rx) = mpsc::unbounded_channel(); // Communication channel between HTTP server to RMQSender 
    let iprt = Arc::new(RwLock::new(HashMap::new())); // inter process routing table hold the link between an HTTP request UUID/correlation ID and the response channel to get the answer back from RMQ
    let pinkie_status = PinkiepieStatus::new();

    // Spawn sending task
    let conf_rmq_sender = conf_rmq.clone();
    let ps_is_ok = pinkie_status.get_is_publisher_ok();
    tokio::spawn( async move {
        if let Err(e) = RMQHandlerPublisher::start_publisher_thread(conf_rmq_sender, sender_rx, ps_is_ok).await {
            log::error!("RMQ sender thread quit on error {}", e);
        }
    });


    // Spawn receiver task
    let conf_rmq_receiver = conf_rmq.clone();
    let iprt_receiver = iprt.clone();
    let ps_is_ok = pinkie_status.get_is_receiver_ok();
    tokio::spawn( async move {
        if let Err(e) = RMQHandlerReceiver::start_receiver_thread(conf_rmq_receiver, iprt_receiver, ps_is_ok).await {
            log::error!("RMQ receiver thread quit on error {}", e);
        }
    });
 
    let handler = HTTPHandler::new(conf, sender_tx, iprt.clone(), pinkie_status);
    log::info!("HTTP server running on {}:{}", &conf_http.address, & conf_http.port);
    Iron::new(handler).http(format!("{}:{}", conf_http.address, conf_http.port)).unwrap();
}

