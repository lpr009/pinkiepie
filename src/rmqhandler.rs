use core::future::Future;
use core::pin::Pin;
use crate::config::*;
use crate::pinkiepieerror::PinkiepieError;
use crate::minirouter::*;
use lapin::{ Connection, Channel, ConnectionProperties, Consumer, ConsumerDelegate}; 
use lapin::options::*;
use lapin::types::{AMQPValue, FieldTable, LongString};
use lapin::message::DeliveryResult;
use tokio::sync::RwLock;
use std::error::Error;
use std::sync::Arc;
use std::collections::HashMap;
use std::convert::*;
use std::string::*;
use tokio::{runtime::Runtime, sync::mpsc, time::{sleep, Duration, timeout}};
use tokio_amqp::*;
use uuid::Uuid;

type Rx = mpsc::UnboundedReceiver<PinkiepieMessage>;
type Iprt = Arc<RwLock<HashMap<String, PinkiepieMessageMetadata>>>;
type PStatus = Arc<RwLock<Result<(), Box<PinkiepieError>>>>;

///
/// Trait for RMQHandler
///
pub struct RMQHandlerHelper {
    _blank: bool // Quirk: async trait is currently not available in Rust we can either use a specific crate (not used) or create an empty struct to store shared function for Receiver and Sender
}

impl RMQHandlerHelper {

	/// Initializes the Connection
    async fn rmq_init_conn(rmq_url: &String, name: &String, source: &String) -> Result<Connection, Box<dyn Error>> {
        let n = name.clone();
        let tr = timeout(Duration::from_secs(5),
                 Connection::connect(rmq_url, ConnectionProperties::default().with_tokio().with_connection_name(LongString::from(n)))
        ).await;
        log::trace!("rmq_init_conn - {} - after timeout", source);
        // Dirty trick because connection freeze during RMQ server start
        match tr {
            Ok(e) => {
                match e {
                    Ok(f) => return Ok(f),
                    Err(f) => return Err(Box::new(PinkiepieError::new(&*format!("{} RMQ connection error: {:?}", source, f))))
                }
            },
            Err(e) => return Err(Box::new(PinkiepieError::new(&*format!("{} try to connect timed out: {}", source, e)))),
        }
    }

    /// Check if connection is still active
    pub async fn rmq_conn_is_active(rmq_conn: &Arc<RwLock<Option<Connection>>>, source: &String) -> bool {
        let mut active = false;

        if let conn =  rmq_conn.clone().read().await {
            match &*conn {
                None => (),
                Some(e) => { if e.status().connected() { active = true } },
            }
        }
        log::trace!("rmq_conn_is_active - {} - {}", source, active);
        active
    }

    /// Checks that the Connection is OK, reinitializes it if it’s not
    pub async fn rmq_check_conn(rmq_url: &String, conn_name: &String, rmq_conn: &Arc<RwLock<Option<Connection>>>, source: &String) -> Result<(), Box<dyn Error>> {
        log::trace!("rmq_check_conn - {} - {}", source, &conn_name);
		if let mut conn =  rmq_conn.clone().write().await{
            log::trace!("rmq_check_conn - {} - {} - got write lock", source, &conn_name);
            match &*conn {
                None => {
                    log::trace!("rmq_check_conn - {} - {} - first init", source, &conn_name);
                    *conn = Some(RMQHandlerHelper::rmq_init_conn(rmq_url, conn_name, &source).await?);
                    log::trace!("rmq_check_conn - {} - {} - first init done", source, &conn_name);
                }
                Some(e) => {
                    log::trace!("rmq_check_conn - {} - {} - do we need to update the conn.", source, &conn_name);
                    if !e.status().connected() {
                        log::trace!("rmq_check_conn - {} - {} - yes please update me", source, &conn_name);
                        *conn = Some(RMQHandlerHelper::rmq_init_conn(rmq_url, conn_name, &source).await?);
                        log::trace!("rmq_check_conn - {} - {} - it’s better now", source, &conn_name);
                    }
                }
            }
        }
        log::trace!("rmq_check_conn - {} - end - {}", source, &conn_name);
    
        Ok(())
    }

    /// Initializes the Channel associated with the Connection
    async fn rmq_init_chan(rabbitmq_ack: bool, rmq_conn: &Arc<RwLock<Option<Connection>>>, source: &String) -> Result<Channel, Box<dyn Error>> {
        log::trace!("rmq_init_chan - {}", source);
        if let conn = rmq_conn.clone().read().await {
            log::trace!("rmq_init_chan - {} - read access granted", source);
            let chan = conn.as_ref().unwrap().create_channel().await?;
            if rabbitmq_ack {
                chan.tx_select().await?;
            }
            log::trace!("rmq_init_chan - {} - end", source);
            return Ok(chan)
        }
        log::trace!("rmq_init_chan - {} - end on error", source);
        Err(Box::new(PinkiepieError::new("RMQ channel unable to initialize")))
    }
	
    pub async fn rmq_chan_is_active(rmq_chan: &Arc<RwLock<Option<Channel>>>, source: &String) -> bool {
        let mut status = false;
        if let chan =  rmq_chan.clone().read().await {
            match &*chan {
                None => (),
                Some(e) => { if e.status().connected() { status = true; } }
            }
        }
        log::trace!("rmq_chan_is_active - {} - {}", source, &status);
        status
    }

	/// Checks that the Channel is OK, reinitializes it if it’s not
    pub async fn rmq_check_chan(rabbitmq_ack: bool, rmq_conn: &Arc<RwLock<Option<Connection>>>, rmq_chan: &Arc<RwLock<Option<Channel>>>, source: &String) -> Result<(), Box<dyn Error>> {
        log::trace!("rmq_check_chan - {}", source);
        if let mut chan =  rmq_chan.clone().write().await {
        log::trace!("rmq_check_chan - {} - write access granted", source);
            match &*chan {
                None => *chan = Some(RMQHandlerHelper::rmq_init_chan(rabbitmq_ack, &rmq_conn, &source).await?),
                Some(e) => {
                    if !e.status().connected() {
                        *chan = Some(RMQHandlerHelper::rmq_init_chan(rabbitmq_ack, &rmq_conn, &source).await?);
                    }
                }
            }
        }

        log::trace!("rmq_check_chan - {} - end", source);
        Ok(())
    }

}

///
/// Implement RMQHandlerPublisher
///
pub struct RMQHandlerPublisher {
    config_rmq: ConfigRMQ,
    rmq_conn: Arc<RwLock<Option<Connection>>>,
    rmq_chan: Arc<RwLock<Option<Channel>>>,
    conn_name: String,
}

impl RMQHandlerPublisher {
    pub fn new(config_rmq: ConfigRMQ) -> RMQHandlerPublisher {
        RMQHandlerPublisher {
            config_rmq: config_rmq,
            rmq_conn: Arc::new(RwLock::new(None)),
            rmq_chan: Arc::new(RwLock::new(None)),
            conn_name: String::from(format!("pinkie-pub-{}", Uuid::new_v4())),
        }
    }

    /// Recover RMQ if needed
    async fn recover(&self) -> Result<(), Box<dyn Error>> {
        let source = String::from("publisher");
        log::trace!("fn recover - {}", source);
        if !RMQHandlerHelper::rmq_conn_is_active(&self.rmq_conn, &source).await {
            RMQHandlerHelper::rmq_check_conn(&self.config_rmq.get_rmq_url(), &self.conn_name, &self.rmq_conn, &source).await?;
        }
        if !RMQHandlerHelper::rmq_chan_is_active(&self.rmq_chan, &source).await {
            RMQHandlerHelper::rmq_check_chan(self.config_rmq.rabbitmq_ack, &self.rmq_conn, &self.rmq_chan, &source).await?;
            log::trace!("fn recover - {} - channel recovered", source);
        }
        Ok(())
    }

    /// Publish a message to rabbitmq
    pub async fn publish(&self, msg: PinkiepieMessage) -> Result<(), Box<dyn Error>> {
        log::trace!("fn publish");
        self.recover().await?;
        log::trace!("fn publish - recovered");

        // Send the message
        if let chan = self.rmq_chan.clone().read().await {
            match &*chan {
                None =>  return Err(Box::new(PinkiepieError::new("RMQ channel unavailable"))) ,
                Some(s) => {
                    match s.basic_publish(self.config_rmq.rabbitmq_exchange.as_deref().unwrap_or(""), 
                                    msg.get_routing_key().as_str(), 
                                    BasicPublishOptions::default(),
                                    msg.get_body().as_bytes().to_vec(),
                                    msg.get_basic_properties() //BasicProperties::default()
                                    ).await {
                        Ok(_e) => { //PublishConfirm
                            return Ok(()) // If needed we can implement a confirmation that RMQ took our message into account
                        }, 
                        Err(e) => return Err(Box::new(PinkiepieError::new(&*format!("Error during publishing to RMQ due to: {}",e)))),
                    }

                }
            }
        }
        // This error message cannot be reached due to RwLock.read()
        Err(Box::new(PinkiepieError::new("Error publish function can’t get channel read")))    
    }

	/// Thread to manage outgoing message to RabbitMQ
    pub async fn start_publisher_thread (conf_rmq: ConfigRMQ, mut rx: Rx, pinkie_status: PStatus) -> Result<(), Box<dyn Error>> {
        let rmq_sender = RMQHandlerPublisher::new(conf_rmq);
        log::debug!("Starting RMQ sender thread");
        loop {
            match rx.recv().await { // Wait for a message to transmit to RMQ
                Some(msg) => {
                    log::trace!("Sender thread received a new message from mpsc");
                    let publish_result = match rmq_sender.publish(msg).await {
                        Ok(e) => { 
                            log::trace!("Sender thread sent message on best effort{:?}", e);
                            Ok(()) 
                        },
                        Err(e) => {
                            log::error!("Sender thread unable to publish message due to: {}", e);
                            Err(Box::new(PinkiepieError::new(&*format!("Sender thread unable to publish message due to: {}", e))))
                        },
                    };
                    // Update PinkiepieStatus
                    let mut ps = pinkie_status.write().await;
                    *ps = publish_result;
                },
                _ => ()
            };
        }
    }

}

///
/// Implement RMQHandlerReceiver
///
pub struct RMQHandlerReceiver {
   config_rmq: ConfigRMQ,
   rmq_conn: Arc<RwLock<Option<Connection>>>,
   rmq_chan: Arc<RwLock<Option<Channel>>>,
   consumer: Arc<RwLock<Option<Consumer>>>,
   iprt: Iprt,
   conn_name: String
}

impl RMQHandlerReceiver {
    pub fn new(config_rmq: ConfigRMQ, iprt: Iprt) -> RMQHandlerReceiver {
        RMQHandlerReceiver {
            config_rmq: config_rmq,
            rmq_conn: Arc::new(RwLock::new(None)),
            rmq_chan: Arc::new(RwLock::new(None)),
            consumer: Arc::new(RwLock::new(None)),
            iprt: iprt,
            conn_name: String::from(format!("pinkie-rec-{}", Uuid::new_v4()))
        }
    }

    /// Recover RMQ if needed
    async fn recover(&self) -> Result<(), Box<dyn Error>> {
        let source = String::from("receiver");
        log::trace!("fn recover - {}", source);
        if !RMQHandlerHelper::rmq_conn_is_active(&self.rmq_conn, &source).await {
            log::trace!("fn recover - {} - on connection error", source);
            RMQHandlerHelper::rmq_check_conn(&self.config_rmq.get_rmq_url(), &self.conn_name, &self.rmq_conn, &source).await?;
            log::trace!("fn recover - {} - on connection error - connection inited", source);
        }
        if !RMQHandlerHelper::rmq_chan_is_active(&self.rmq_chan, &source).await {
            log::trace!("fn recover - {} - on channel error", source);
            RMQHandlerHelper::rmq_check_chan(self.config_rmq.rabbitmq_ack, &self.rmq_conn, &self.rmq_chan, &source).await?;
            self.rmq_init_queue().await?;
            self.rmq_init_consumer().await?;
        }
        log::trace!("fn recover - {} - end", source);
        Ok(())
    }

    /// Public function to call recover
    pub async fn start(&self) -> Result<(), Box<dyn Error>> {
        self.recover().await
    }

    /// Initialize queue
    /// RMQ queue implementation is idempotent thus every call do the same things
    async fn rmq_init_queue(&self) -> Result<(), Box<dyn Error>> {
        if let chan = self.rmq_chan.clone().read().await {
            let mut ft = FieldTable::default();
            ft.insert("x-expires".into(), AMQPValue::LongInt(60000)); // RPC queue expire after

            chan.as_ref().unwrap().queue_declare(&self.config_rmq.rabbitmq_rpc_queue,
                                                 QueueDeclareOptions { 
                                                     passive: Default::default(), 
                                                     durable:  false, 
                                                     exclusive: Default::default(),
                                                     auto_delete: true,
                                                     nowait: Default::default() },
                                                 ft).await?;
        }
        Ok(())
    }

    async fn rmq_init_consumer(&self) -> Result<(), Box<dyn Error>> {
        if let mut cons = self.consumer.clone().write().await {
             if let chan = self.rmq_chan.clone().read().await {
                 
                 let bco = BasicConsumeOptions {
                     no_local: false,
                     no_ack: true, // Quirk find out a solution to this Dirty fix: ack in the Delegate on_new_delivery function create a deadlock.
                     exclusive: false,
                     nowait: false
                 };

                 *cons = Some(chan.as_ref().unwrap().basic_consume(&self.config_rmq.rabbitmq_rpc_queue,
                                                                   &format!("pinkie-c-{}", Uuid::new_v4()),
                                                                   bco, //BasicConsumeOptions::default(),
                                                                   FieldTable::default()).await?);
                 cons.as_ref().unwrap().set_delegate(RMQReceiverDelegate::new(self.iprt.clone()))?;
             }
        }
        Ok(())
    }

	/// Thread to manage incoming message from RabbitMQ
	pub async fn start_receiver_thread(conf_rmq: ConfigRMQ, iprt: Iprt, pinkie_status: PStatus) ->  Result<(), Box<dyn Error>> {
	    log::debug!("Starting RMQ receiver thread");
	    let rmq_receiver = RMQHandlerReceiver::new(conf_rmq, iprt);
	    log::trace!("Starting RMQ receiver thread - init");
	    rmq_receiver.start().await?; 
	    log::trace!("Starting RMQ receiver thread - started");
	    loop {
	        sleep(Duration::from_secs(60)).await;
	        log::debug!("Recover receiver consumer if needed");
	        let receiver_result = match rmq_receiver.recover().await {
	            Ok(()) => {
                    log::trace!("Recover receiver succeeded");
                    Ok(())
                },
	            Err(e) => {
                    log::error!("Recover error {:?}", e);
                    Err(Box::new(PinkiepieError::new(&*format!("Receiver thread unable to recover due to: {}", e))))
                },
	        };
            // Update PinkiepieStatus
            let mut ps = pinkie_status.write().await;
            *ps = receiver_result;
	    }
	}

}

/// RMQReceiverDelegate forward message from RMQ to the HTTP client
struct RMQReceiverDelegate {
    iprt: Iprt 
}

impl RMQReceiverDelegate {
    pub fn new(iprt: Iprt) -> RMQReceiverDelegate {
        RMQReceiverDelegate {
            iprt: iprt
        }
    }
}

impl ConsumerDelegate for RMQReceiverDelegate {
    fn on_new_delivery(&self, delivery: DeliveryResult) -> Pin<Box<dyn Future<Output = ()> + Send>> {
       let delivery = delivery.expect("Error caught in consumer");
       let mut content_type = None;

       if let Some(delivery) = delivery {
            let rwlock = self.iprt.clone();

            /*
             * Quirk: This code freeze the application.
             * Linked to rmq_init_consumer dirty fix (disable ack)
             *
            let b = delivery.1.acker.clone();
            Runtime::new().unwrap().block_on( async move {
                b.ack(BasicAckOptions::default()).await;
            });
            */

            if let Some(e) = delivery.1.properties.content_type().as_ref() {
                log::trace!("on_new_delivery content type: {}", e.to_string());
                content_type = Some(e.to_string());
            }

            if let Some(correlation) = delivery.1.properties.correlation_id().as_ref() {
                let uuid = correlation.to_string();
                let c_uuid = uuid.clone();

                // Quirk: how to make use of Tokio framework in the delegate? It actually does not
                // seems to be a Tokio task…
                let o_sender = Runtime::new().unwrap().block_on( async move { 
                    let mut iprt = rwlock.write().await;
                    iprt.remove(&c_uuid)
                });

                /*match o_sender {
                    None => { log::error!("No reference in the IPRT for : {}", &uuid); },
                    Some(e) => { e.resp_tx.send(Some(PinkiepieMessageResponse::new(uuid, 
                                                                      Some(delivery.1.data),
                                                                      content_type)
                                        )).expect("Error transmitting message from RabbitMQ to HTTP server");
                    }
                };*/

                match o_sender {
                    None => { log::error!("No reference in the IPRT for : {}", &uuid); },
                    Some(e) => { match e.resp_tx.send(Some(PinkiepieMessageResponse::new(uuid, 
                                                                      Some(delivery.1.data),
                                                                      content_type)
                                        )) {
                        Ok(_) => (),
                        Err(_) => log::error!("Error transmitting message from RabbitMQ to HTTP server")
                    }
                    }
                };
            }
        } 
        
        Box::pin(async { () })
    }

}
